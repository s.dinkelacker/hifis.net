---
title: HIFIS for Scientists
title_image: default
layout: services/default
additional_css:
  - image-scaling.css
excerpt: >-
    No text.
---


**HIFIS supports scientific projects** with IT resources, services, consulting and expertise from the collection, storage, linking and analysis of research data to the publication of the results.
Particular focus is placed on the topic of research software development.

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="Poster Element 1: We are starting a scientific Project. Can you help us?"
        src="{{ site.directory.images | relative_url }}/hifisfor/poster-1.svg"
    />
    </div>
    <div class="text-box">
        <h3>Set up and plan your projects</h3>
        <p>
            HIFIS provides a multitude of Helmholtz Cloud Services that are extremely useful to
            <b>set up the collaboration environment with your scientific partners</b>.
        <p></p>
            For starters, these services include versatile tools for
            document sharing,
            collaborative editing,
            event management,
            chat,
            survey creation and analysis,
            as well as
            project management.
        </p><p>
            The complete list and all further information can be found at the
            <a href="https://helmholtz.cloud/services"><i class="fas fa-external-link-alt"></i><b>
            Helmholtz Cloud Portal</b></a>.
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="Poster Element 2: Data gathering, processing, testing. That's how you automate testing."
        src="{{ site.directory.images | relative_url }}/hifisfor/poster-2.svg"
    />
    </div>
    <div class="text-box">
        <h3>Data gathering, processing and testing</h3>
        <p>
            Collect and process your data with HIFIS provided
            <a href="https://helmholtz.cloud/services?search=comput"><i class="fas fa-external-link-alt"></i><b>
            Compute and Storage Ressources</b></a>.
            HIFIS sustainably <b>protects
            sensitive data</b> by <b>storing and processing all data in Helmholtz-owned data centres</b>.
            Test and automate your workflows using best practices in 
            <a href="{% link services/software/helmholtzgitlab.html %}"><b>
            Continuous Integration and Deployment</b></a>,
            in order to increase reliability and development speed.
            <p></p>
            Have a look at our tutorials on our
            <a href="{% link services/overall/learning-materials.md %}"><b>
            Learning Materials
            </b></a>
            on best-practices of Software Engineering.
            Further, you are invited to check out our 
            <a href="https://events.hifis.net/category/4"><b><i class="fas fa-external-link-alt"></i>
            HIFIS Education &amp; Training
            </b></a>
            programme, aiming at employees who are looking for further trainings
            in the field of research software development.
            Both entry-level and advanced courses are available.
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="Poster Element 3: Data consolidation: Sustainability. We support you with courses, consulting and collaboration tools."
        src="{{ site.directory.images | relative_url }}/hifisfor/poster-3.svg"
    />
    </div>
    <div class="text-box">
        <h3>Consolidation of your Research</h3>
        <p>
            Today, there is hardly a scientific project that can be done without the support of scientific software.
            Be it software packages developed in-house or third-party applications which are used for the acquisition of scientific knowledge:
            The quality, traceability, and reproducibility of research depend to a large extent on those software packages.
            HIFIS therefore supports research software development with a wide range of services.
        </p><p>
            <a href="{% link consulting.md %}"><b>
            HIFIS Consulting
            </b></a>
            is an individual and free offer for all researchers of the Helmholtz Association.
            We will help you with a broad range of topics around research software engineering,
            open source,
            or questions about the implementation of new or existing software projects.
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="Poster Element 4: Data Re-use. We help you to transfer your results."
        src="{{ site.directory.images | relative_url }}/hifisfor/poster-4.svg"
    />
    </div>
    <div class="text-box">
        <h3>Publication and Re-use</h3>
        <p>
        HIFIS provides a services for sustainably making the results of your research data accessible.
        This includes a 
        <a href="https://helmholtz.cloud/services?serviceDetails=svc-9e9284f7-f768-4611-bd1f-d977b8327b1b"><i class="fas fa-external-link-alt"></i><b>
        Cloud Service for Publishing Research Data and Metadata
        </b></a>,
        with DOI/PID indexing, as well as a (yet prototypic)
        <a href="https://hifis.net/doc/cloud-services/pilot-services/prototype-dcache/"><b>
        Storage Service</b></a>.
        Our <a href="{% link consulting.md %}"><b>
        Consulting Service
        </b></a>
        can help you with questions on licensing and re-use of your software projects.
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="HIFIS Logo, claim. Contact us anytime at support@hifis.net"
        src="{{ site.directory.images | relative_url }}/hifisfor/HIFIS_logo_claim_blue.svg"
    />
    </div>
    <div class="text-box">
        <h3>Queries, comments and suggestions</h3>
        <p>
            If you have suggestions or questions, please do not hesitate to contact us.
        </p>
        <p>
            <a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="image-attrib">Images by <a href="https://lornaschuette.com">Lorna Sch&uuml;tte</a>.</div>
