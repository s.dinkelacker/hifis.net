const allCollapsibles = document.getElementsByClassName("collapsible");

for(let collapsible of allCollapsibles) {
    collapsible.onclick = function() {
        const funderInfo = this.nextElementSibling;
        if (funderInfo.style.display === "none") {
            funderInfo.style.display = "block";
            this.classList.remove("closed");
            this.classList.add("opened");
        } else {
            funderInfo.style.display = "none";
            this.classList.remove("opened");
            this.classList.add("closed");
        }
    }
};