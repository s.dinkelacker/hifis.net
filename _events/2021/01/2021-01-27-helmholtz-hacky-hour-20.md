---
title: "Helmholtz Hacky Hour #20"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-01-27"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Why don't you share your source code?</strong> Concerns about and solutions for common problems with code sharing."
---
## Why don't you share your source code?
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

We want to talk about occurring problems with publishing software, how to solve them and why it is important to publish research software

We are looking forward to seeing you!
