---
title: "Machine Learning Hackathon at GFZ" 
layout: event
organizers:
  - "Machine Learning community at GFZ"
type: Hackathon
start:
    date:   "2020-03-05"
    time:   "09:00"
end:
    date:   "2020-03-05"
    time:   "18:00"
location:
    campus: "GFZ, Telegrafenberg"
    room:   "Vortragsraum 2 and 3, Haus H"
excerpt:
    " A one day Hackathon organized by Machine Learning Community at GFZ "
---

This hackathon aims to bring researchers that are experts in machine learning and/or intersted researchers together. Using real world datasets, it allows researchers to work together using existing machine learning tools and methods. The hackathon is a friendly event that let everyone to share experience and learn from others.
