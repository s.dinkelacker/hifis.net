---
title: HIFIS - Helmholtz Federated IT Services
title_image: default
layout: frontpage
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
additional_js: frontpage.js
notification:
  title: "DESY Cloud Services Maintenance"
  description: "On November 8th, 8-18 o'clock CET, the DESY Openstack Cloud will undergo maintenance.
  Most DESY Helmholtz Cloud services will not be available then.
  HIFIS Events Service and DESY Sync&Share may be available if you log in before the maintenance time window."
excerpt:
  "HIFIS aims to provide excellent, seamlessly accessible IT services for the
  whole Helmholtz Association."
---
{% comment %}
  This markdown file triggers the generation of the frontpage.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
