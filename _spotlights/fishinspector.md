---
layout: spotlight

name: FishInspector
date_added: 2022-07-11
preview_image: fishinspector/FishInspector_logo.png

excerpt: >
  The software FishInspector provides automatic feature detections in images of
  zebrafish embryos (body size, eye size, pigmentation). It is Matlab-based and
  provided as a Windows executable (no matlab installation needed).

title_image: FishInspector.jpg
title: FishInspector
keywords:
    - image feature detection
    - pattern recognition
    - phenotype screening
hgf_research_field: Earth & Environment
hgf_centers:
    - Helmholtz Centre for Environmental Research (UFZ)
contributing_organisations:
    - name: TKS3 - Scientific Software Solutions
      link_as: http://www.tks3.de/
# List of scientific communities
scientific_community:
# Impact on community (optional, not implemented yet)
impact_on_community:
contact: stefan.scholz@ufz.de
platforms:
    - type: webpage
      link_as: https://www.ufz.de/index.php?de=44460
    - type: github
      link_as: https://github.com/sscholz-UFZ/FishInspector
license: GPL-3.0-or-later
costs: free
software_type:
    - image feature detection
application_type:
    - Desktop
programming_languages:
    - MATLAB
doi: 10.1093/toxsci/kfy250
funding:
    - shortname: Federal Ministry of Education and Research
      link_as: https://www.bmbf.de/bmbf/shareddocs/bekanntmachungen/de/2015/12/1124_bekanntmachung
---

# FishInspector

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/fishinspector/FishInspector.jpg" alt="FishInspector v1.03 screenshot">
<span>Screenshot of FishInspector Software Version 1.03</span>
</div>

The FishInspector software allows the user-friendly and easy annotation of
features in zebrafish embryo 2-dimensional images. Features are detected
automatically but may require correction by the user (implemented in the
software). At present only lateral images can be analysed, but the software
could be extended to analyse dorsoventral images as well. The software has been
initially developed for images obtained with an automated capillary
position system. However, images from other sources can be used as well after
appropriate conversion (see [here](https://www.ufz.de/index.php?de=44460)). During the installation process an
appropriate version of MATLAB runtime will be installed as well. No full
MATLAB installation is required. FishInspector has been tested on Windows 7 but
may run on other Windows versions as well.

More details on the software and the subsequent data analysis can be found in
the following publication:

[*Teixido, E., Kießling, T.R., Krupp, E., Quevedo, C., Muriana, A., and Scholz, S. (2019): Automated morphological feature assessment for zebrafish embryo developmental toxicity screens. Tox. Sci. 167(2), 438–449*](https://academic.oup.com/toxsci/advance-article/doi/10.1093/toxsci/kfy250/5123521)

## Plugins, future versions

The software is intended to be modified and extended by the community. If you
are interested or have a special interest please do not hesitate to contact
[us](mailto:stefan.scholz@ufz.de).
