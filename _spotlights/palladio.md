---
layout: spotlight

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------
name: Palladio

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-06-27

preview_image: palladio/palladio_preview.png
excerpt: >
    Palladio is a software architecture simulation approach which analyses
    software at the model level for performance bottlenecks, scalability issues,
    reliability threats, and allows for subsequent optimisation.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: Palladio

# Add at least one keyword
keywords:
    - Modeling
    - Simulation
    - Software Architecture
    - Performance Prediction
    - Component-Based Software Engineering

# The Helmholtz research field
hgf_research_field: Information

# At least one responsible centre
hgf_centers:
    - Karlsruhe Institute of Technology (KIT)

# List of other contributing organisations (optional)
contributing_organisations:
    - name: Karlsruhe Institute of Technology (KIT)
      link_as: https://www.kit.edu/
    - name: FZI Research Center for Information Technology
      link_as: https://www.fzi.de/
    - name: University of Stuttgart
      link_as: https://www.uni-stuttgart.de/

# List of scientific communities
scientific_community:

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: palladio-dev@ira.uni-karlsruhe.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: github
      link_as: https://github.com/PalladioSimulator
    - type: webpage
      link_as: https://www.palladio-simulator.com/

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: EPL-2.0

# Is the software pricey or free? (optional)
costs: free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - modeling
    - simulation
    - performance prediction
    - quality prediction

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - Java

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi:

# Funding of the software (optional)
funding:
    - shortname: KIT
    - shortname: DFG
    - shortname: State of Baden-Württemberg
---

# Palladio  - Modeling and Simulating Software Architectures

Palladio is a software architecture simulation approach which analyses
your software at the model level for performance bottlenecks,
scalability issues, reliability threats, and allows for a subsequent
optimisation. Palladio requires neither buying expensive executions
environments nor fully implementing a software product.

Palladio comprises:

* The Palladio Component Model (PCM), a detailed meta-model of
  component-based software architectures.
* A component-based software development process. Palladio is aligned
  with this process and enables distributed modeling for
  component-based software development scenarios.
* A software architecture simulator: Multiple performance,
  reliability, maintainability, and cost prediction approaches are
  combined. The approaches, by means of analysis or simulation,
  calculate metrics (e.g. response time) from Palladio models
  (instance of the PCM).
* Tool support, the Palladio-Bench implements all aspects of Palladio.
  The Palladio-Bench is extendable such that it can serve as
  implementation base for new scientific directions.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/palladio/palladio_bench_screenshot1.png" alt="Palladio-Bench Overview">
<span>Palladio-Bench Overview (system view, behaviour view, simulation run, performance results).</span>
</div>

## Analysis

Palladio supports a broad range of analysis scenarios. Each scenario
can be analysed with respect to performance, reliability,
maintainability, and costs. Palladio reduces the risks and implied
costs of picking the wrong design decisions by a model-based
engineering approach.

Typical software architecture analysis scenarios are:
* Sizing
* Scalability
* Load Balancing
* Configuration Optimization
* Design Alternatives
* Extension of Legacy Software


## Tools

We have implemented an integrated modelling environment (called
Palladio-Bench) based on the Eclipse IDE. It enables developers to
create Palladio Component Model (PCM) instances with graphical editors
and derive performance, reliability, maintainabilty, and cost metrics
from the models using analytical techniques and simulation. The
Palladio Component Model (PCM) is implemented using the Eclipse
Modeling Framework (EMF). Using the Palladio-Bench, you model PCM
instances, simulate models, view simulation results, and derive
software design optimisations.

The best way for getting started with the Palladio-Bench is looking
through the screencasts and tutorials provided on our [official
website](https://www.palladio-simulator.com/).

## Research

Palladio is a well-validated approach for the prediction of Quality of
Service (QoS) properties of component-based software architectures. It
enables the creation of high quality software architectures with
dependable quality properties.

Numerous publications demonstrate the applicability of Palladio for
scientific and real-world industrial scenarios of different domains.
The book ["Modeling and Simulating Software Architectures -- The
Palladio Approach"](https://www.palladio-simulator.com/the_book/)
presents the underlying methodology and background. Ongoing
development effort and the continuous integration of latest scientific
trends provides cutting-edge features to researchers and
practictioners. More information on the scientific background of
Palladio and developer information can be found in the [Palladio
Developer
Wiki](https://sdqweb.ipd.kit.edu/wiki/Palladio_Component_Model).
