---
title: "HIFIS has a newsletter!"
title_image: paula-hayes-Eeee5H-yuoc-unsplash.jpg
data: 2022-07-29
authors:
  - klaffki
layout: blogpost
categories:
  - News
tags:
  - Newsletter
excerpt: >
    Subscribe to our newsletter to get the second edition in october!
---

## We just published the first issue of our quarterly HIFIS Newsletter

<figure>
    <img src="{% link assets/img/hifisfor/poster-3.svg %}" alt="two people talking about figures on flipchart" style="float:right;width:7%;min-width:100px;padding:5px 0px 20px 20px;">
</figure>

If you missed it, you can read the [HTML]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.md %}) or [PDF version]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.pdf %}) of the July issue.
Within our newsletter, you can meet Sam Scientist who will accompany us from now on and tell us about their experiences with HIFIS.
The newsletter features the latest engaging community use cases that show how HIFIS services contribute to and supporte scientific challenges.

Of course, there are also news in the literal sense in each issue, informing you about important current developments within HIFIS.
There's also a section for information from HIFIS Education, advertising the next course or sharing some tips and tricks.
You will also find news and interesting content from the Helmholtz Cloud or HIFIS Software.
And, last but not least, we share current information or calls from other Incubator plattforms and
provide a link to the job search for data science jobs at Helmholtz, curated by HIDA. 


## Don't want to miss the next newsletter?
Subscribe [here](https://lists.desy.de/sympa/subscribe/hifis-newsletter) to get the second edition in october and 
all further newsletters automatically!
The growing archive will be kept [here]({% link newsletter/index.md %}).
