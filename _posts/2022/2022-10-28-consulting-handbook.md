---
title: "HIFIS Consulting Handbook is online"
title_image: jingda-chen-4F4B8ohLMX0-unsplash.jpg
data: 2022-10-28
authors:
  - ravindran
  - foerster
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Consulting
excerpt: >
    The HIFIS Consulting Handbook is NOW publicly available. All information about the consulting process and building your consulting infrastructure can be found there.
---

# HIFIS Consulting Handbook 

<div class="floating-boxes">
<div class="image-box align-right">
<img
  class="right medium nonuniform"
  alt="DataHub logo"
  src="{% link assets/img/posts/2022-10-28-consulting-handbook/undraw_book.svg %}"
  style="float:right;width:20%;min-width:320px;padding:5px 5px 20px 20px;"
/>
</div>
<div class="text-box" markdown="1">

If your organisation does not already have a consulting team for your researchers, setting up and implementing one can be difficult.
What policies do you include? 
What will the consulting process look like? 
Should you have helpful resources? 
If so, which ones? How do you make it 100% online?
How do you obtain feedback on the consulting provided?

What if that's what you are stuck on?

We answered a few of these questions in our [HIFIS Consulting Handbook](https://hifis.net/consulting-handbook/).
It focuses on explaining how to set up and run a consulting team and leaves the specifics for the IT infrastructure setup and fine-grained procedures up to you. 
Helmholtz institutes in Germany started the HIFIS Consulting service in 2020 to cater to the needs of every non-IT researcher.

This handbook is made from our experiences and working with different scientists, helping them from cleaning their code and optimising to publishing.

It's a great read; we highly recommend you check it out for inspiration!
</div>

<span class="team-card-mini" style="
    display: flex;
    align-items: center;
    justify-content: center;"
>
  <a 
    href="https://hifis.net/consulting-handbook/"
    class="btn btn-outline-primary">
      <i class="fas fa-book-open"></i>
      &nbsp;&nbsp;Link to the HIFIS Consultation Handbook
  </a>
</span>
</div>

## Feedback
We are looking forward to your feedback!
Please to not hesitate to [contact us](mailto:support@hifis.net).
