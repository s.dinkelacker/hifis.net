---
title: "HIFIS Software Helpdesk launched"
date: 2020-06-01
authors:
  - ziegner
layout: blogpost
title_image: default
categories:
  - Announcement
tags:
  - general
  - support
redirect_from:
  - news/2020/06/01/hifis-software-helpdesk-launched
excerpt:
    "
    We are happy to announce the launch of the HIFIS Software Helpdesk - the
    place where your user requests for HIFIS Software Services are handled by
    our distributed team.
    "
---

We are happy to announce the launch of the 
**[HIFIS Software Helpdesk][helpdesk]** - the place where
_your_ user requests for HIFIS Software Services are handled by our distributed
team.
The helpdesk solution is based on the open source helpdesk/customer
support system [Zammad][zammad].

- You need more support for your scientific software project?
- You have software related questions you want answered?
- You are looking for a specific workshop?
- You can’t find exactly what you’re looking for?

Let us know and create a ticket in our shiny new [helpdesk][helpdesk] or
send an email to [{{ site.contact_mail }}][contact_mail], which will
result in the same.
Tickets aren't sent to one individual person, but to the whole team.
They are then forwarded to the most relevant expert and will stay open until
they are resolved.

## Do I need to create an account for the helpdesk?
No, since the helpdesk is registered within the Helmholtz Authentication and
Authorization Infrastructure (AAI) you can simply sign in using your personal
access data of your home institution or even your personal [GitHub][github]
account.
All you have to do is to use the respective login button for the Helmholtz
AAI sign-in method on the login page.

[contact_mail]: mailto:{{ site.contact_mail }}
[github]: https://github.com/
[helpdesk]: https://{{ site.helpdesk }}/
[zammad]: https://zammad.org/
