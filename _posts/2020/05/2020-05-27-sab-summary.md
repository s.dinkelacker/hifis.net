---
title: "HIFIS Scientific Advisory Board: First Feedback Report"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2020-05-27
authors:
  - jandt
layout: blogpost
categories:
  - News
excerpt: >
    In April, the HIFIS SAB held its first meeting as a video conference. Members of SAB compiled a comprehensive feedback report.

---

# First SAB report

In April 2020, the HIFIS Scientific Advisory Board gathered in a video conference to discuss the progress of HIFIS and provide advice. 
Comments and suggestions were compiled in a comprehensive report.

### Summary

The  Scientific  Advisory  Board  (SAB)  of  the  newly  established  platform  “Helmholtz  Federated  IT Services” (HIFIS) held its constitutive meeting as a virtual conference on April 29, 2020. 
The HIFIS annual  platform  report  2019  and  platform  proposal  were  provided  beforehand,  as  well  as presentation slides and materials on the cloud service selection after the meeting.

During  the  meeting,  Helmholtz  representatives  and  HIFIS  members  held  presentations  providing insight  on  ambitions  and  actual  progress  of  HIFIS  in  the  context  of  Helmholtz  and  the  European research   landscape. 
These   presentations interchanged   with   open   discussion   sessions   of   all participants. 
Minutes of the discussion have been provided to the SAB members a few days after the meeting.

The SAB members were given additional opportunity to provide written feedback within approx. oneweek  after  the  meeting,  allowing  to  assemble  this  collective  feedback  report.  There  were  many positive comments and reactions in regard to HIFIS and its progress. 

#### Specific points to highlight
- **Positioning of HIFIS as a platform, not a project.** The presentations were refreshing in that they  were  from  a  scientific  user’s  perspective  and  not  overly  focused  on  technical specifications.  HIFIS  has  a  flexible  architecture  that  anticipates  specific  systems  being integrated and also decommissioned or replaced. The emphasis is on building a sustainable resource that works well for researchers.  This perspective and positioning should continue, although some experts would enjoy additional specifications for review.
- **Integration with other platforms.** HIFIS’s utility grows exponentially as it is integrated withother scientific platforms.  This work is necessary, needed, and should be prioritized.
- **Integration into the proposal process.** Related to the point above, it is recommended thatHIFIS  will  be  given  improved  visibility  and  insight  in  the  proposal  process  of  Helmholtz platform  projects  they  hope  to  integrate  with. The  benefits  of  coordination  and  early knowledge can reap numerous benefits.  For example, even without requiring new work, the HIFIS team could factor in the technology and data choices of funded projects on the AI andimaging platforms, so HIFIS is better aligned to serve or integrate in later.
- **Collaboration with international initiatives.** Both HIFIS as a groundbreaking platform and the Helmholtz Association have much to share with the international scientific community. As well, by connecting with international initiatives, especially European ones, HIFIS may find other work to build upon, that will increase capacity for additional work. Specific feedback given by six experts from the SAB are given in full detail in the following sections.

### Full Report

Specific feedback given by six experts from the SAB are given in full detail in the [**full report**](https://nubes.helmholtz-berlin.de/f/84005457) (limited access).
