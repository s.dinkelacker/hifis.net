---
title: "Super-fast AI Computing System for HAICORE"
title_image: default
date: 2020-07-08
authors:
  - jandt
layout: blogpost
categories:
  - News
excerpt_separator: <!--more-->
lang: en
lang_ref: 2020-07-08-haicore-kit
---

A super-fast computer system dedicated to [Helmholtz artificial intelligence (AI)](https://www.helmholtz.ai/) research has been installed at [KIT](https://www.kit.edu). 
This is the first location in Europe to put the InfiniBand connected NVIDIA DGX A100 AI system into operation, 
funded from the Helmholtz AI Computing Resources Initiative (HAICORE). 
Common access to these capacities is established using the AAI service offered by HIFIS.
<!--more-->

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.kit.edu/kit/english/pi_2020_056_super-fast-ai-system-installed-at-kit.php">
  <i class="fas fa-external-link-alt"></i> Read more
</a>
