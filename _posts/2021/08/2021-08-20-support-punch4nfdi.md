---
title: "Helmholtz AAI supports PUNCH4NFDI"
title_image: branimir-balogovic-fAiQRv7FgE0-unsplash.jpg
data: 2021-08-24
authors:
  - "Sander Apweiler"
  - "Marcus Hardt"
  - "Uwe Jandt"
layout: blogpost
redirect_from: news/use%20case/2021/08/20/support-punch4nfdi.html
categories:
  - News
excerpt: >
    The Helmholtz AAI supports PUNCH4NFDI by providing AAI components. This offers a working solution on a strongly connected AAI, which follows already the recommendations and guidelines for participating in the EOSC.
---

# Support for PUNCH4NFDI

The Helmholtz AAI supports [PUNCH4NFDI](https://www.punch4nfdi.de/) by providing AAI components.
PUNCH4NFDI is the NFDI consortium of particle, astro-, astroparticle, hadron and nuclear physics.
It is one of [19 funded NFDI consortiums](https://www.dfg.de/en/research_funding/programmes/nfdi/funded_consortia/index.html).

In the Helmholtz AAI, PUNCH4NFDI has its own branded endpoint for user authentication to services.
The user management is done jointly with the Helmholtz AAI.
This allows the consortium to exploit an AAI without repeating the evolution of an identity management on their own.
It offers a working solution on a strongly connected AAI, which follows already the recommendations and guidelines for participating in the [European Open Science Cloud (EOSC)](https://www.eosc.eu).

The Helmholtz AAI supports multiple communities beyond the Helmholtz Association, which strengthens the Helmholtz AAI in the operation with other community or research infrastructure AAIs.


### Comments? Feedback?

If you need assistance or want to give feedback, please **contact us at <mailto:support@hifis.net>**.

---

#### Changelog
- 2022-02-23 -- This article was originally posted on [aai.helmholtz.de](https://gitlab.hzdr.de/helmholtz-aai/aai.helmholtz.de/-/blob/2d3ba7224073775b11e4391ec8f9ff1f5dc2b637/_posts/2021/08/2021-08-20-support-punch4nfdi.md) and has been slightly reformatted for this site.
