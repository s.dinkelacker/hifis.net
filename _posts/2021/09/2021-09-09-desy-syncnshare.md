---
title: "DESY Sync & Share service available to Helmholtz"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2021-09-09
authors:
  - jandt
  - "van der Reest, Peter"
layout: blogpost
categories:
  - News
excerpt: >
    DESY contributes its Sync &amp; Share Service to the Helmholtz Cloud, for usage by Helmholtz and collaborators!
    The service, based on Nextcloud Enterprise, is an online storage service, to get easy and fast access to all of your stored data.

---

## DESY Sync & Share

{{ page.excerpt }}

DESY has been offering Sync & Share services since 2013.

#### Features

* **Access your data on the web:**
  Save your own data in DESY Sync & Share and access it from everywhere via web browser.
* **Synchronize your data:**
  Store all your data fully synced on all your devices to work smoothly and barrier-free.
* **Integrate your cloud data in your desktop environment:**
  Mount DESY Sync & Share on your desktop via WebDav Drive.
* **Work on shared documents collaboratively:**
  The OnlyOffice environment allows joint work on Office documents and other formats.
* DESY Sync and Share uses the **dCache storage system** to store your data in a highly resilient and redundant manner, while offering principally unlimited storage capacity.
* Additional features, like collaborative calendar, may be made available upon [request](mailto:support@hifis.net?subject=[DESYSNS]).

Refer to [DESY Service Documentation](https://it.desy.de/services/storage_services/desy_sync__share/) for full documentation and FAQs.

#### Usage Conditions

Visit the Helmholtz Cloud Portal at [**helmholtz.cloud** <i class="fas fa-external-link-alt"></i>](https://helmholtz.cloud/services) to access the service and to check usage details.

In brief, individual users from Helmholtz ([with IdP connected to Helmholtz AAI](https://hifis.net/doc/backbone-aai/list-of-connected-centers/)) can use the service straightaway.
User groups from Helmholtz need to briefly apply for usage by sending a request to support.
Again: See Cloud Portal for details!

#### Access using Helmholtz AAI 

No additional accounts or passwords are required, as the access is mediated through the Helmholtz AAI.
Using this infrastructure, you and all colleagues can use your **home institution’s login** to use the Sync & Share service seamlessly.
Furthermore, group management is performed through the AAI as well.

* For a [detailed tutorial on how the basic access works, have a look here!]({% link aai/howto.md %})
* If you have successfully applied for a group, you may also [refer here for group management in the AAI](https://hifis.net/doc/helmholtz-aai/howto-vo-management/).

## Further services in Helmholtz Cloud

Until end 2021, multiple more Helmholtz Cloud services are expected to be added.
Stay tuned to not miss any!

## Comments and Suggestions

If you have questions need assistance, please do not hesitate to contact.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>
