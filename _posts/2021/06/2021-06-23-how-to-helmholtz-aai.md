---
title: "AAI login to Helmholtz Cloud and Associated Services"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2021-06-23
authors:
  - "Uwe Jandt"
layout: blogpost
categories:
  - Tutorial
tags:
  - Helmholtz AAI
excerpt: >
    Pictured tutorial on usage of Helmholtz AAI login and registration procedures for Helmholtz Cloud and Associated services, using the HIFIS Events Management Platform at <a href="https://events.hifis.net">events.hifis.net</a> as an example.
---

# Login to Helmholtz Cloud and Associated Services via Helmholtz AAI

Helmholtz AAI allows seamless access to a large and growing number of Cloud Services, supported by [HIFIS](https://hifis.net).

The Services are distinguished in:
* [**Associated Services**](https://login.helmholtz.de/unitygw/VAADIN/files/connected-services.html),
* [**Helmholtz Cloud Services**](https://cloud.helmholtz.de/): The services are accessible through the Helmholtz Cloud Portal (beta).

#### Exemplary Cloud Service: HIFIS Events Management Platform

As a popular example, the [Event Management Platform](https://events.hifis.net/) of HIFIS has recently been
[made available](https://hifis.net/news/2021/04/27/announce-hifis-event-platform)
for all Helmholtz and collaboration partners.
It is based on CERN's [Indico](https://getindico.io/) tool.

### Easy login via your username at your home institute

Like for all Cloud Services, you can **use your home institution's login** to use the Events Platform seamlessly.

Unlike some other Indico instances (and the old HIFIS instance), specific Indico accounts are not necessary anymore and also not possible. 
This is managed via the [Helmholtz AAI]({% link aai/index.md %}) (Authentication and Authorisation Infrastructure).

(As a fallback, a login from, e.g., ORCID, can also be performed. You might also try Github, or Google.)

### Click-by-Click: Login to the HIFIS Event Management Platform via Helmholtz AAI

<i class="fas fa-lightbulb"></i> _Click to open screenshots for each step._

<details>
<summary>Open the <strong>HIFIS Event Management Platform</strong> in your browser: <a href="https://events.hifis.net">https://events.hifis.net</a>.</summary>
<div markdown=1>
{:.treat-as-figure}
![URL to service]({% link assets/img/how-to-aai/events.hifis.net/0_url_to_service.png %})
</div>
</details>

<details>
<summary>Click on <strong>Login</strong> at the top right.</summary>
<div markdown=1>
{:.treat-as-figure}
![Login button on events.hifis.net]({% link assets/img/how-to-aai/events.hifis.net/1_events_login.png %})
</div>
</details>

<details>
<summary>Click on <strong>Helmholtz AAI</strong>.</summary>
<div markdown=1>
{:.treat-as-figure}
![Keycloak login page, click on Helmholtz AAI]({% link assets/img/how-to-aai/events.hifis.net/2_keycloak.png %})
</div>
</details>

<details>
<summary>Select your home institution, i.e. the institution where you already have your home account. 
You can scroll down or use filtering, e.g. <i>helmholtz</i> or <i>desy</i>.
The example is about DESY.
You have to find your own Helmholtz Centre or institution.
</summary>
<div markdown=1>
{:.treat-as-figure}
![Choice of institutions in Unity]({% link assets/img/how-to-aai/events.hifis.net/3_unity_institutions.png %})
</div>
</details>

<details>
<summary>Provide your username, passphrase for your home institution. There is no new password or username required.</summary>
<div markdown=1>
{:.treat-as-figure}
![Login at home IdP]({% link assets/img/how-to-aai/events.hifis.net/4_home_idp.png %})
</div>
</details>

<details>
<summary>Accept data transmissions from your home institution's identity provider (IdP) to Helmholtz AAI, and further to events.hifis.net.</summary>
<div markdown=1>
{:.treat-as-figure}
![Consent collection]({% link assets/img/how-to-aai/events.hifis.net/5a_consents_collected.png %})
</div>
</details>

<details>
<summary>Only if you log in the first time -- <strong>Register at Helmholtz AAI</strong>.</summary>
<div markdown=1>
* Click on **Register**.
* Please check your name and mail address.
* Read and accept the AAI usage policies.   
    You may choose to remember your decision, so you won't be bothered again.  
    If you later decide otherwise, you can revoke consent and delete stored data [here](https://login.helmholtz.de/home/).
* Click on **Submit**.

{:.treat-as-figure}
![Unity Registration]({% link assets/img/how-to-aai/events.hifis.net/5b_unity_registration.png %})
</div>
</details>

<details>
<summary>Continue with normal log-in and enjoy access to to <a href="https://events.hifis.net">events.hifis.net</a>.</summary>
<div markdown=1>
{:.treat-as-figure}
![Successfully logged in to events.hifis.net]({% link assets/img/how-to-aai/events.hifis.net/6_events_logged_in.png %})
</div>
</details>


### Having problems with login to a Cloud Service?

If you have persisting problems, please **contact us at <support@hifis.net>**, providing:
* The exact service (and possibly sub-page) you want to access,
* The exact error your receive,
* The URL / address of the error page,
* The time point of your (failed) login attempt.
