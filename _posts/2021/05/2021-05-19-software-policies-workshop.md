---
title: "Policies for Research Software"
title_image: sign-pen-business-document-48148.jpg
data: 2021-05-19
authors:
  - Messerschmidt, Reinhard
layout: blogpost
categories:
  - News
excerpt: >
  At the workshop Policies for Research Software held on May 6, 2021,
  over 100 participants from Helmholtz centers showed great interest.
  A productive exchange of ideas following Wolfgang Marquardt's welcoming
  address and the keynote speeches in four Zoom breakout sessions was
  initiated.

---

At the workshop _[Policies for Research Software][event-link]_ held on May 6, 2021,
over 100 participants from Helmholtz centers showed great interest.
The intended target group (primarily Policy makers, software development team
leaders e.g. from data centers, infrastructure units, science management,
libraries, law and technology transfer or scientific software communities)
was represented in full breadth.
This made it possible to initiate a productive exchange of ideas following
Wolfgang Marquardt's welcoming address and the keynote speeches in four
breakout sessions.

<figure>
  <a href="{% link assets/img/posts/2021-05-19-software-policies-workshop/miro_board.png %}">
    <img src="{% link assets/img/posts/2021-05-19-software-policies-workshop/miro_board.png %}" width="50%" alt="Whiteboard of the parellel sessions">
  </a>
  <figcaption>Whiteboard of one of the parallel sessions.</figcaption>
</figure>

## Workshop Material

The results were documented on virtual whiteboards.
All presentation slides and the result of the virtual whiteboards are made
available in [Nubes](https://nubes.helmholtz-berlin.de) upon request.

## Future Exchange Process
All interested Helmholtz stakeholders are very welcome to actively participate
in the exchange process following the workshop to collaboratively create a
checklist for the development and introduction of a policy in the centers.
Therefore, a communication channel is introduced.
We encourage you to join.

<a href="https://mattermost.hzdr.de/signup_user_complete/?id=t1rh7xaksbnfpb3z7d5bahym5y" markdown=1>**<i class="fas fa-comments"></i> &nbsp;Click here to join the Mattermost chat.**</a>{:.btn .btn-primary}

**The Research Software Forum is looking forward to a productive exchange with you!**

#### Comments and Suggestions

If you have suggestions, questions, or queries, please don't hesitate to write us.

<a href="{% link contact.md %}" 
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk"> 
                            Contact us! <i class="fas fa-envelope"></i></a>

[event-link]: https://os.helmholtz.de/veranstaltungen/foren/1-forum-forschungssoftware/
