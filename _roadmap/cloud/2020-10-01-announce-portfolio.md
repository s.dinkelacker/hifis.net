---
date: 2020-10-01
title: Tasks in October 2020
service: cloud
---

## Announcement of initial Helmholtz Cloud Service Portfolio
After multiple iterations, the first set of selected federated services, forming the initial Helmholtz
Cloud Service Portfolio, will be announced and started to be integrated. This set will be continuously enhanced
and the performance of the integrated services will be monitored.
