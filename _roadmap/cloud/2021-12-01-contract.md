---
date: 2021-12-01
title: Tasks in Dec 2021
service: cloud
---

## Legal Framework
The legal framework describes the necessary coordination and responsibilities so that the Helmholtz Cloud can perform its task for and within the Helmholtz community. It covers the relevant aspects, such as data sovereignty, intended use, GDPR or IT security.
