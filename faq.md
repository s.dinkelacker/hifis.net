---
title: <i class="fas fa-question-circle"></i> FAQ — Frequently Asked Questions
title_image: default
layout: default
excerpt:
    Collection of Frequently Asked Question (FAQ) about HIFIS.
---

## General Questions about HIFIS
{:.text-success}

<details class="my-3">
    <summary>
        <strong id="what-is-the-mission-of-hifis">What is the mission of HIFIS?</strong>
    </summary>
    <div>
        <b>Our Aim</b>
    <p>The top position of Helmholtz research is increasingly based on cross-centre and international cooperation and common access to data treasure and -services.
At the same time, the significance of a sustainable software development for the research process is recognised.</p>

<p><b>HIFIS builds and sustains an excellent IT infrastructure connecting all Helmholtz research fields and centres.</b></p>

<p>This includes the Helmholtz Cloud, where members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use. HIFIS further supports Research Software Engineering (RSE) with a high level of quality, visibility and sustainability.</p>

<p>The HIFIS clusters develop technologies, processes and policy frameworks for harmonized access to fundamental backbone services, such as Helmholtz AAI, and Helmholtz Cloud services. HIFIS Software also provides education, technology, consulting and community services.</p>

<p><b>Helmholtz Digital Services for Science — Collaboration made easy.</b></p>
        <p><i class="fas fa-headphones"></i>
        <strong>You are invited to <a href="https://resonator-podcast.de/2021/res172-hifis/">tune in to the recent Resonator podcast on HIFIS</a>, to get an idea on our aims and scope!</strong> (German only)
        <i class="fas fa-headphones"></i></p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="timeline">What is the timeline for HIFIS?</strong>
    </summary>
    <div>
        <p>The HIFIS platform was set up in 2019, with basic infrastructure services (AAI) and pilot services online since 2020. The <a href="{% post_url 2020/10/2020-10-13-initial-service-portfolio %}">initial cloud service portfolio has been made public</a> by
        end of 2020;
        the production version of the <a href="{% post_url 2021/11/2021-11-17-CloudPortal %}">Helmholtz Cloud Portal was made available</a> by November 2021.
        The finalisation of the cloud rulebook and GDPR compliant agreement is planned during 2022.</p><p>
        Please also refer to our <a href="{{ "roadmap" | relative_url }}"><strong>HIFIS Roadmap</strong></a> to stay up to date.
        </p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="where-can-i-register-for-a-newsletter">Where can I register for a newsletter?</strong>
    </summary>
    <div>
        <p>Since July 2022, HIFIS is sending quarterly newsletters via <code class="language-plaintext highlighter-rouge">newsletter@hifis.net</code>. You are invited to register to never miss any news and upcoming events! Newsletters published so far can be <a href="{% link newsletter/index.md %}">found here</a>.</p>
        <p>Either register <a href="mailto:sympa@desy.de?subject=sub%20hifis-newsletter">by sending a registration mail</a> or
        <a href="https://lists.desy.de/sympa/subscribe/hifis-newsletter">register here</a>, where you can also unregister.</p>
        <p>To stay updated on general HIFIS news, you are invited to check out
        <a href="{{ site.feed.collections.posts.path | relative_url }}">our RSS news feed</a>—besides, of course, regularly checking our website and <a href="news">news section!</a></p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="how-should-i-acknowledge-hifis-assistance-in-a-publication">How should I acknowledge HIFIS assistance in a publication?</strong>
    </summary>
    <div>
        <p>Please include the following text:</p>
        <blockquote>
        <p>We gratefully acknowledge the HIFIS (Helmholtz Federated IT Services)
        team for … (e.g. support with the <a href="#how-can-hifis-help-me-as-a-researcher">components above</a>)</p>
        </blockquote>
    </div>
</details>


## Access and Service Portfolio
{:.text-success}

<details class="my-3">
    <summary>
        <strong id="how-can-i-access-cloud-services">How can I access Cloud Services?</strong>
    </summary>
    <div>
        <p>Find all our Cloud Services, as well as any usage details and conditions at the
        <a href="https://helmholtz.cloud/services"><strong>Helmholtz Cloud Portal</strong></a>.</p>
        <p>It is easier than ever before, since you do not need to create new accounts with new passwords!
        Just search for “Helmholtz AAI” when logging in and use your home institute’s credentials.</p>
        <p>Please
        <a href="{% link aai/howto.md %}"><strong>refer to our illustrated tutorial on how to access Cloud Services via Helmholtz AAI</strong></a>
        for more details.</p>
    </div>
</details>

<details class="my-3">
    <summary>
        <strong id="my-helmholtz-center-is-not-directly-involved-into-hifis-do-you-still-help-me">My Helmholtz center is not directly involved into HIFIS. Do you still help me?</strong>
    </summary>
    <div>
        <p>Yes, of course. HIFIS is a Helmholtz-wide platform that aims to
        provide offers for <strong>all</strong> Helmholtz centers.
        Please <a href="mailto:support@hifis.net">contact us</a> or any of the <a href="{% link team.md %}">HIFIS team members</a> to get assistance.</p>
    </div>
</details>

## HIFIS for Scientists
{:.text-success}
<details class="my-3">
    <summary>
        <strong id="how-can-hifis-help-me-as-a-researcher">How can HIFIS help me as a researcher?</strong>
    </summary>
    <div>
        <p>The purpose of HIFIS is to support science and scientists. Within the HIFIS platform, we are building:</p>
        <ul>
        <li>The <a href="https://helmholtz.cloud"><strong>Helmholtz Cloud</strong></a>, providing a portfolio of seamlessly accessible IT services to simplify your daily work.</li>
        <li>All cloud services and software trainings and support are free of charge!</li>
        <li>A <strong>Helmholtz-wide login mechanism</strong>, the <a href="{% link aai/index.md %}">Helmholtz AAI</a>,</li>
        <li>A stable, high-bandwidth <strong>network infrastructure</strong> connecting all Helmholtz centers,</li>
        <li><a href="{{ "services/software-overview" | relative_url }}"><strong>Software services</strong></a> to provide you with a common platform,
        <a href="{% link events.md %}">training</a> and
        <a href="{% link services/software/consulting.html %}">support</a> for high-quality sustainable software development.</li>
        </ul>
    </div>
</details>

## Helmholtz AAI
{:.text-success}
<details class="my-2">
<summary><strong>What is Helmholtz AAI?</strong></summary>
<div markdown=1>
AAI is short for "Authentication and Authorisation Infrastructure" and comprises the central component in gaining access to a service shared by a Helmholtz centre different from your home centre. Once you log in to the Cloud Portal itself or the federated services available through it, it will ask for your home centre. When you choose it, you will get redirected to your home identity provider (IdP), enter your username and password there and get authenticated at the service. The HelmholtzAAI manages the communication between your home IdP and the federated service you want to access. No hassle for you.
</div>
</details>

<details class="my-2">
<summary><strong>I accidentally refused to transfer my personal data when trying to log in to a service. Now I cannot use it. How to revert that?</strong></summary>
<div markdown=1>
- Go to <https://login.helmholtz.de/home>
- Log in, if needed
- On the account page, click on _Preferences_ and then select the _OAuth_ preferences tab.
- In the list, select the service of interest and either delete the entry or edit the settings.
</div>
</details>

<details class="my-2">
<summary><strong>While logging in using my home organisation, I got an “Authentication failed” error.</strong></summary>
<div markdown=1>
The error message says: "The remote authentication was successful, however the the server's policy requires more information then was provided to register your account."

Your home organisation did not release all mandatory information to create your Helmholtz AAI account. Beside the federations metadata, the mandatory information that your organisation needs to transfer to the Helmholtz AAI can be reviewed in the [documentation](https://hifis.net/doc/helmholtz-aai/attributes/#consumed-attributes-from-identity-providers-idp). Please contact your organisation's helpdesk and ask to release the mandatory information to the Helmholtz AAI. In case your home organisation does not want to release this information, you can use some social accounts like Google, GitHub and ORCID.
</div>
</details>

<details class="my-2">
<summary><strong>While logging in, I got an “Authorization Server got an invalid request / No OAuth context” error.</strong></summary>
<div markdown=1>
Login via Helmholtz AAI requires the communication between multiple instances that must not be interrupted. If the login flow is paused or gets interrupted, the session might get lost or the exchanged information between the different services is not valid anymore. When the login is continued, the AAI service can not resume the session or use the received information if they are outdated. In these cases, it shows the message "Authorization server got an invalid request. No OAuth context." Please restart the login from the service you want to use and do not pause or interrupt it. If the error appears although there was no pause, please contact us at <support@hifis.net>.
</div>
</details>

<details class="my-2">
<summary><strong>How does the authentication via Helmholtz AAI work?</strong></summary>
<div markdown=1>
In short: the central component of the HelmholtzAAI is a Unity instance that serves as a proxy between your home identity provider (IdP) and the service you want to access. Your home IdP knows who you are and is trusted by the HelmholtzAAI. You just need to log in with your username and password of your center at your home IdP and the rest is magic. :-)

- Have a look at the [**illustrated tutorial on how this works**]({% post_url 2021/06/2021-06-23-how-to-helmholtz-aai %})!
- For more details, you are invited to have a look at the [concept page of Helmholtz AAI](https://hifis.net/doc/helmholtz-aai/concepts/).
</div>
</details>

<details class="my-2">
<summary><strong>How does the authorisation at the services work -or- how does a service know what I am allowed to do there?</strong></summary>
<div markdown=1>
Either your home identity provider or the HelmholtzAAI (via a virtual organisation) communicate your permissions (so-called entitlements) to the service.
</div>
</details>

<details class="my-2">
<summary><strong>What is a virtual organisation?</strong></summary>
<div markdown=1>
A virtual organisation (VO) is a group of people led by a principal investigator (PI) that can gain special access to services if they have specialised needs for more features than a basic variant of a service offers. They can also negotiate and gain access to services with a restricted access policy. All this, however, is based on individual agreements.
</div>
</details>

<details class="my-2">
<summary><strong>Which Helmholtz centres are connected to HelmholtzAAI?</strong></summary>
<div markdown=1>
All connected centres are listed here: <https://hifis.net/doc/helmholtz-aai/list-of-connected-organisations/> . At the moment of writing, all centres except GSI and DZNE were connected.
</div>
</details>

<details class="my-2">
<summary><strong>How do I create a new virtual organisation?</strong></summary>
<div markdown=1>
A new virtual organisation has to be requested by a principal investigator (PI) at the HIFIS team. Note that the PI has to be a member of a Helmholtz centre. The procedure for reqeusting a new virtual organisation is shown on <https://hifis.net/doc/helmholtz-aai/howto-vos/>.
</div>
</details>

<details class="my-2">
<summary><strong>Where do I find further information?</strong></summary>
<div markdown=1>
Have a look at [**the HIFIS and AAI documentation page**](https://hifis.net/doc), more technical information on all our topics is there. If you don't find what you are looking for, contact us at <support@hifis.net>.
</div>
</details>
